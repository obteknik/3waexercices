<?php

namespace App\Controller\ex2;

use App\Entity\Module;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ModuleController extends AbstractController
{
    /**
     * @Route("/modules", name="modules")
     */
    public function modules()
    {
        //Liste des modules
        $modules= $this->getDoctrine()->getRepository(Module::class)->findAll();

        return $this->render('ex2/module/modules.html.twig', [
            'modules' => $modules
        ]);
    }

    /**
     * Ajouter un module à une formation
     * @Route("/addModule", name="add_module")
     */
    public function add()
    {        
        
        return $this->render('ex2/module/addModule.html.twig', [
           
        ]);
    }

    /**
     * éditer un module
     * @Route("/editModule/{id}",name="edit_module")
     */
    public function edit($id)
    {

        //redirection
        return $this->redirectToRoute('modules');
    }

    /**
     * supprimer un module
     * @Route("/deleteModule/{id}",name="delete_module")
     */
    public function delete($id)
    {

        //redirection
        return $this->redirectToRoute('modules');
    }

    /**
     * consulter un module
     * @Route("/module/{id}",name="view_module")
     */
    public function view($id)
    {

        $module = $this->getDoctrine()->getRepository(Module::class)->find($id);

        return $this->render('ex2/module/viewModule.html.twig', [
            'module' => $module 
        ]);
    }

   /**
     * consulter la liste des formations disposant du module
     * @Route("/module/{id}/trainings",name="view_module_trainings")
     */
    public function viewModuleTrainings($id)
    {

        $module = $this->getDoctrine()->getRepository(Module::class)->find($id);

        return $this->render('ex2/module/viewModuleTrainings.html.twig', [
            'module' => $module 
        ]);
    }
}
