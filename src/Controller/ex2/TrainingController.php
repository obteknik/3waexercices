<?php

namespace App\Controller\ex2;

use App\Entity\Training;
use App\Entity\Module;
use App\Form\TrainingType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TrainingController extends AbstractController
{

    /*public function bdd(){


    }*/

    /**
     * Home
     * @Route("/home", name="home")
     */
    public function home()
    {

        ////////////////////////////////////////////////////////////////
        //création des formations avec modules en utilisant addModule()
 /*       $em = $this->get('doctrine')->getManager();

        //Création des formations      
        //FORMATION 1
        $formation1 = new Training;
        $formation1->setName('BackOffice Web Developper');
        $formation1->setSlug('backoffice-webdevelopper');
        $formation1->setDescription('Lorem1');
        $em->persist($formation1);

        //FORMATION 2
        $formation2 = new Training;
        $formation2->setName('Front-end Web Developper');
        $formation2->setSlug('Front-end-WebDevelopper');
        $formation2->setDescription('Lorem2');
        $em->persist($formation2);

        //FORMATION 3
        $formation3 = new Training;
        $formation3->setName('Web Designer');
        $formation3->setSlug('Web-Designer');
        $formation3->setDescription('Lorem3');
        $em->persist($formation3);
        
        //Création des modules
        $module1 = new Module();
        $module1->setName('HTML CSS');
        $em->persist($module1);

        $module2 = new Module();
        $module2->setName('PHP');
        $em->persist($module2);
 
        $module3 = new Module();
        $module3->setName('MYSQL');
        $em->persist($module3);

        $module4 = new Module();
        $module4->setName('HTML');
        $em->persist($module4);

        $module5 = new Module();
        $module5->setName('JavaScript');
        $em->persist($module5);

        $module6 = new Module();
        $module6->setName('Bootstrap');
        $em->persist($module6);

        $module7 = new Module();
        $module7->setName('Photoshop');
        $em->persist($module7);

        $module8 = new Module();
        $module8->setName('Illustrator');
        $em->persist($module8);

        //Rattachement des modules par formation
        //1
        $formation1->addModule($module1);
        $formation1->addModule($module2);
        $formation1->addModule($module3);
        //2
        $formation2->addModule($module1);
        $formation2->addModule($module5);
        $formation2->addModule($module6);
        //3
        $formation3->addModule($module1);
        $formation3->addModule($module7);
        $formation3->addModule($module8);
        $formation3->addModule($module6);
        
        $em->flush();*/
        ////////////////////////////////////////////////////////////////

        //Liste des formations
        $trainings= $this->getDoctrine()->getRepository(Training::class)->findAll();

        return $this->render('ex2/home.html.twig', [
            'trainings' => $trainings
        ]);
    }

    /**
     * Liste des formations
     * @Route("/trainings", name="trainings")
     */
    public function trainings()
    {

        $trainings = $this->getDoctrine()->getRepository(Training::class)->findAll();

        return $this->render('ex2/training/trainings.html.twig', [
            'trainings' => $trainings 
        ]);
    }

    /**
     * Création des formations
     * @Route("/add", name="add")
     */
    public function add(Request $request)
    {
        $training = new Training();

        //Création du formulaire
        $form = $this->createForm(TrainingType::class,$training);

        //Traitement du formulaire
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if($form->isValid()){
                
                $em = $this->get('doctrine')->getManager();

                $em->persist($training);
                $em->flush();

                //message flash
                $this->addFlash('notice','The training has been created !');
               
                //redirection
                return $this->redirectToRoute('home');
            }
        } 

        return $this->render('ex2/training/add.html.twig', [
            'form' => $form->createView() 
        ]);
    }

    /**
     * Modification des formations
     * @Route("/edit/{slug}", name="edit")
     */
    public function edit(Request $request,$slug)
    {
        $data = $this->getDoctrine()->getRepository(Training::class)->findBySlug($slug);

        $training = $data[0];

        //Création du formulaire
        $form = $this->createForm(TrainingType::class,$training);

        //Traitement du formulaire
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if($form->isValid()){
                
                $em = $this->get('doctrine')->getManager();

                $em->persist($training);
                $em->flush();

                //message flash
                $this->addFlash('notice','The training has been updated !');
               
                //redirection
                return $this->redirectToRoute('trainings');
            }
        } 

        return $this->render('ex2/training/add.html.twig', [
            'form' => $form->createView() 
        ]);
    }

    /**
     * Suppression d'une formation
     * @Route("/delete/{slug}", name="delete")
     */
    public function delete(Request $request,$slug)
    {

        $this->addFlash('notice','The training has been deleted !');
            
        return $this->redirectToRoute('home');
    }

    /**
     * Ajout d'un module à une formation
     * @Route("/addModule/{slug}", name="add_module_training")
     */
    public function addModuleToTraining()
    {        
        
        return $this->render('ex2/training/addModuleToTraining.html.twig', [
           
    ]);

    }

    /**
     * consulter une formation
     * @Route("/training/{slug}",name="view")
     */
    public function view($slug)
    {

        $training = $this->getDoctrine()->getRepository(Training::class)->findBySlug($slug);

        return $this->render('ex2/training/view.html.twig', [
            'training' => $training 
        ]);
    }
}
