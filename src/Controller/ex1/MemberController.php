<?php
/**
 * /*************** 1e Exercice de 3WAcademy **************
*Intitulé
*Affichage d'une liste de membres
*Description
*Chaque membre possède un identifiant - sous forme de slug ou numérique - un nom, un prénom, un email et une photo
*
*Une première page doit présenter uniquement les noms et prénoms de tous les membres, puis en cliquant sur un bouton, une seconde page présente 
* l'ensemble de ses informations
*Informations techniques
*La première route doit être de la forme "/members"
*La seconde route doit être de la forme "/member/ID" où « ID » est l'identifiant unique du membre
*Fichiers fournis
*La mise en page des vues réalisées à l'aide de Bootstrap*/

namespace App\Controller\ex1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Member;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
  * @Route("/ex1")
*/
class MemberController extends AbstractController
{
    /**
     * @Route("/members", name="members")
     */
    public function index()
    {

        //Récupération des membres
        $members = $this->getDoctrine()->getRepository(Member::class)->findAll();

        return $this->render('ex1/index.html.twig', [
            'members' => $members
        ]);
    }

    /**
     * @Route("/member/{id}", name="member_show", requirements={"id"="\d+"})
     */
    public function show($id)
    {

        $member = $this->getDoctrine()->getRepository(Member::class)->find($id);

        if (!$member) {
            throw $this->createNotFoundException('Identifiant de membre inconnu.');
        }

        return $this->render('ex1/detail.html.twig', [
            'member' => $member
        ]);
    }
}
