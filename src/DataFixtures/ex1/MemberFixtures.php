<?php

namespace App\DataFixtures\ex1;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Member;
use Faker\Factory;

class MemberFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->faker =factory::create();
        $this->addFaker($manager);
        $manager->flush();
    
    }

    private function addFaker(ObjectManager $manager){

        $faker = Factory::create('fr_FR');

        $members=[];

        for ($i=0; $i < 25; $i++) {
            $members[$i] = new Member();
            $members[$i]->setLastname($faker->lastname)
                        ->setFirstname($faker->firstname)
                        ->setEmail($faker->email)
                        ->setPhoto($faker->imageUrl($width = 640, $height = 480));

            $manager->persist($members[$i]);
          }
    }
}

